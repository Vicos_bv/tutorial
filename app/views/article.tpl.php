<?php require_once("header.tpl.php") ?>
<div id="content">
    <!-- Content -->
    <!-- Post -->
    <div class="block">
        <div id="post" class="cell3">
            <h2><?php echo $post['title'] ?></h2>
            <p class="post_info">
                <span><?php echo $post['email'] ?></span>
                <span><?php echo date('Y-m-d H:i:s' , $post['created_at']) ?></span>
            </p>
            <div>
                <img class="leftimg" src="<?php echo $post['image'] ?>">
                <p>
                    <?php echo $post['content'] ?>
                </p>
            </div>
        </div>
    </div>
    <!-- End Post -->
    <!-- Comments -->
    <div class="block">
        <div class="cell3">
            <h3>Comments</h3>
            <hr>
            <?php
            if(!$comments) {
                ?>
                <div class="comment">
                    <h4>You can be the first to comment this post</h4>
                </div>
            <?php
            } else {
                foreach($comments as $comment) {
                    ?>
                    <!-- Comment -->
                    <div class="comment">
                        <div class="comment_head block">
                            <div class="wrap_head">
                                <h3><?php echo $comment['email'] ?></h3>
                                <img src="<?php echo $comment['avatara'] ?>">
                                <span><?php echo date('Y-m-d H:i:s' , $comment['created_comment']) ?></span>
                            </div>
                        </div>
                        <div class="text_comment">
                            <p>
                                <?php echo $comment['content'] ?>
                            </p>
                        </div>
                        <div class="clear"></div>
                        <hr>
                    </div>
                    <!-- End Comment -->
                <?php }
            }?>
        </div>
    </div>
    <!-- End Comments -->
    <div class="block">
        <div class="cell3">
            <div id="form_comment">
                <h3>Leave a comment</h3>
                <form action="crud/add_comment.php" method="post">
                    <?php if ($_SESSION['errorValidEmail']) { ?>
                        <div class="error_block_comment"><p>Invalid email address</p></div>
                    <?php } ?>
                    <div id="name_and_email">
                        <input type="email" name="email" value="<?php echo $arrayDataComment[0] ?>" placeholder="Email*" pattern="^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$">
                        <input type="hidden" name="id_post" value="<?php echo $_GET['id'] ?>">
                    </div>
                    <?php if ($_SESSION['errorAddComment']) { ?>
                        <div class="error_block_comment"><p>Invalid text</p></div>
                    <?php } ?>
                    <textarea id="text_comment" name="text_comment" rows="4"><?php echo $arrayDataComment[1] ?></textarea>
                    <br>
                    <div>
                        <button id="button_submit" type="submit" class="btn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="block">

        <div class="clear"></div>
    </div>
    <div class="hfooter"></div>
</div>
<!-- End Content -->
</div><!--wrap-->
<?php require_once("footer.tpl.php") ?>