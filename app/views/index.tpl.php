<?php require_once("header.tpl.php") ?>
<div id="content">
    <!-- Content -->
    <div class="block">
        <!-- Posts -->
        <?php
        foreach($result as $post) {?>
            <div class="post">
                <!-- Post -->
                <div class="image cell1">
                    <img src="<?php echo $post['image'] ?>">
                </div>
                <div class="text cell2">
                    <h2><a href="<?php $_SERVER['REQUEST_URI'] ?>/index.php?q=article&id=<?php echo $post['id'] ?>"><?php echo $post['title'] ?></a></h2>
                    <p class="post_info">
                        <span><?php echo $post['email'] ?></span>
                        <span><?php echo date('Y-m-d H:i:s' , $post['created_at']) ?></span>
                    </p>
                    <p>
                        <?php echo $post['excerpt'].'...' ?>
                    </p>
                    <a class="btn" href="<?php $_SERVER['REQUEST_URI'] ?>/index.php?q=article&id=<?php echo $post['id'] ?>">Read more</a>
                </div>
                <!-- End post -->
            </div>
        <?php } ?>
        <!-- End Posts -->
    </div>
    <div class="hfooter"></div>
    <!-- End Content -->
</div>
</div>
<!--wrap-->
<?php require_once("footer.tpl.php") ?>