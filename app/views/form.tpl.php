<?php require_once("config/conn.php") ?>
<?php require_once("config/lib.php")?>
<?php require_once("header.tpl.php") ?>
    <div id="content">
        <!-- Content -->
        <!-- Form       -->
        <div id="form_register" class="block">
            <form action="">
                <div class="cell3">
                    <div class="cell1">
                        <input type="text" name="name" class="_form" id="name_form" placeholder="Name"/>
                    </div>
                    <div class="cell1">
                        <input type="email" name="email" class="_form" id="email_form" placeholder="E-mail"/>
                    </div>
                    <div class="cell1">
                        <input type="password" name="pass" class="_form" id="pass_form" placeholder="******"/>
                    </div>
                </div>
                <div class="block">
                    <div class="cell1"><input type="submit" class="_form btn" id="btn_form"/></div>
                </div>
            </form>
        </div>
        <!-- End Form       -->
        <div class="block">

            <div class="clear"></div>
        </div>
        <div class="hfooter"></div>
    </div>
    <!-- End Content -->
    </div><!--wrap-->
<?php require_once("footer.tpl.php") ?>