<?php

class Helper
{
    /**
     * @return Loader
     */
    public static function getLoader()
    {
        static $loader;

        if($loader) return $loader;

        return $loader = new Loader();
    }
}