<?php

/**
 * This class is designed to work with the database
 */
class Database
{
    /**
     * Connect to database
     * @param default array $params
     */
    public function __construct($params)
    {
        // Connect to database from $params
    }

    /**
     * Database query
     * @param string $query
     * @return array|bool
     */
    public function query($query /*TODO maybe more params*/)
    {
        // Escape special characters
        // Run query
        //return boolean || array();
    }
}