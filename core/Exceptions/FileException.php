<?php

class FileException extends Exception
{
    public function __construct($message = 'You have an error in your php code.')
    {
        $this->message = $message;
    }

    //TODO
    public function __toString()
    {
        //return "exception '".__CLASS__ ."' with message '".$this->getMessage()."' in ".$this->getFile().":".$this->getLine()."\nStack trace:\n".$this->getTraceAsString();
    }
}