<?php


class DbException extends Exception
{
    public function __construct($message = 'default message')
    {
        $this->message = $message;

    }

    //TODO
    public function __toString()
    {
        //return "exception '".__CLASS__ ."' with message '".$this->getMessage()."' in ".$this->getFile().":".$this->getLine()."\nStack trace:\n".$this->getTraceAsString();
    }
}