<?php require_once("../config/conn.php") ?>
<?php require_once("../config/lib.php") ?>
<?
/**
 * Add Comment
 */
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $email = clearStr($_POST['email']);
    $textComment = clearStr($_POST['text_comment']);
    $idPost = clearInt($_POST['id_post']);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL) || empty($email)) {
        $_SESSION['errorValidEmail'] = 1;
    }
    if (empty($textComment)) {
        $_SESSION['errorAddComment'] = 1;
    }
    if (isset($_SESSION['errorValidEmail']) || isset($_SESSION['errorAddComment'])) {
        $arrayDataComment = array($email, $textComment);
        $arrayDataComment = json_encode($arrayDataComment);
        $_SESSION['dataComment'] = $arrayDataComment;
        header("Location: http://" . $_SERVER["SERVER_NAME"] . "/index.php?q=article&id=" . $idPost);
    } else {
        $result = addComment($email, $textComment, $idPost);
        if ($result === true) {
            header("Location: http://" . $_SERVER["SERVER_NAME"] . "/index.php?q=article&id=" . $idPost);
        } else {
            header("Location: http://" . $_SERVER["SERVER_NAME"] . "/index.php?q=article&id=" . $idPost);
        }
    }
} else {
    header("Location: http://" . $_SERVER["SERVER_NAME"] . "/index.php?q=index");
}
?>