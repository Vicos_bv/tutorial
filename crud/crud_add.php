<?php require_once("../config/conn.php") ?>
<?php require_once("../config/lib.php") ?>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $email = clearStr($_POST['email']);
    $titlePost = clearStr($_POST['title_post']);
    $postText = clearStr($_POST['field_text']);
    $image = clearStr($_POST['image']);
    $arrayDataPost = array('email' => $email,
        'titlePost' => $titlePost,
        'fieldText' => $postText,
        'image' => $image);
    $arrayDataPost = json_encode($arrayDataPost);

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $_SESSION['validEmail'] = '<div class="error_block"><p>Invalid email addresses</p></div>';
    }
    if (empty($titlePost)) {
        $_SESSION['emptyTitle'] = '<div class="error_block"><p>Must not be empty</p></div>';
    }
    if (empty($postText)) {
        $_SESSION['emptyPost'] = '<div class="error_block"><p>Must not be empty</p></div>';
    }
    if (!filter_var($image, FILTER_VALIDATE_URL)){
        $_SESSION['validUrl'] = '<div class="error_block"><p>Invalid url addresses</p></div>';
    }
    if (!empty($email) && !empty($titlePost) && !empty($postText) && !empty($image)) {
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) && filter_var($image, FILTER_VALIDATE_URL)) {
            $resAddPost = addPost($email, $titlePost, $postText, $image);
            $_SESSION['successAdd'] = '<div class="success_block"><p>Your post was successfully added</p></div>';
            header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=index");
        } else {
            $_SESSION['dataPost'] = $arrayDataPost;
            header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=add");
        }
    } else {
        $_SESSION['dataPost'] = $arrayDataPost;
        header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=add");
    }
} else {
    header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=index");
}
?>