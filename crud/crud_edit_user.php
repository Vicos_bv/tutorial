<?php require_once("../config/conn.php") ?>
<?php require_once("../config/lib.php") ?>
<?php
/**
 * Edit User
 */
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $idUser = clearInt($_POST['user_id']);
    $email = clearStr($_POST['email']);
    $image = clearStr($_POST['image']);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL) || empty($email)) {
        $_SESSION['errorValidEmail'] = '<div class="error_block"><p>Failed to update profile</p></div>';
    }
    if (!filter_var($image, FILTER_VALIDATE_URL) || empty($email)) {
        $_SESSION['errorValidUrl'] = '<div class="error_block"><p>Failed to update profile</p></div>';
    }
    if (isset($_SESSION['errorValidEmail']) || isset($_SESSION['errorValidUrl'])) {
        header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=edit_user&edit=" . $idUser);
    }
    if ($_POST['update'] === "Submit") {
        $result = editUser($idUser, $email, $image);
        if ($result === true) {
            $_SESSION['successUpdateUser'] = '<div class="success_block"><p>Profile has been successfully updated</p></div>';
            header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=users");
        } else {
            $_SESSION['errorUpdateUser'] = '<div class="error_block"><p>Failed to update profile</p></div>';
            header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=edit_user&edit=" . $idUser);
        }
    } else {
        header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/");
    }
} else {
    header("Location: http://" . $_SERVER["SERVER_NAME"] . "/crud/index.php?q=index");
}

?>